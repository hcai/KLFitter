/*
 * Copyright (c) 2009--2018, the KLFitter developer team
 *
 * This file is part of KLFitter.
 *
 * KLFitter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * KLFitter is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with KLFitter. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KLFITTER_RESDOUBLeGAUSS_MET_H_
#define KLFITTER_RESDOUBLEGAUSS_MET_H_

#include <vector>

#include "KLFitter/ResDoubleGaussBase.h"

// ---------------------------------------------------------

/**
 * \namespace KLFitter
 * \brief The KLFitter namespace
 */
namespace KLFitter {
/**
  * \class KLFitter::ResGauss_MET
  * \brief A class describing a Gaussian resolution, parametrized for MET.
  *
  * This class offers a simple parameterization of a resolution. The
  * parameterization is a Gaussian with a width of a constant times the
  * square root of the true parameter.
  */
class ResDoubleGauss_MET : public ResDoubleGaussBase {
 public:
  /** \name Constructors and destructors */
  /* @{ */

  /**
    * The default constructor.
    */
  explicit ResDoubleGauss_MET(const char * filename);

  /**
    * A constructor.
    * @param parameters The parameters of the parameterization.
    */
  explicit ResDoubleGauss_MET(std::vector<double> const& parameters);

  /**
    * The (defaulted) destructor.
    */
  ~ResDoubleGauss_MET();

  /* @} */
  /** \name Member functions (Get)  */
  /* @{ */


  /**
    * Return the probability of the true value of x given the
    * measured value, xmeas.
    * @param x The true value of x.
    * @param xmeas The measured value of x.
    * @param good False if problem with TF.
    * @param sumet SumET, as the width of the TF depends on this.
    * @return Logarithm of the probability.
    */
  double logp(double x, double xmeas, bool *good, double sumet) override;

  /* @} */
  /** \name Member functions (Set)  */
  /* @{ */

  /**
    * Calculate the mean of the first Gaussian from the TF parameters and the value of x.
    * @param x The value of x.
    * @return The width.
    */
  double GetMean1(double x) override;

  /**
    * Calculate the width of the first Gaussian from the TF parameters and the value of x.
    * @param x The value of x.
    * @return The width.
    */
  double GetSigma1(double x) override;

  /**
    * Calculate the amplitude of the second Gaussian from the TF parameters and the value of x.
    * @param x The value of x.
    * @return The width.
    */
  double GetAmplitude2(double x) override;

  /**
    * Calculate the mean of the second Gaussian from the TF parameters and the value of x.
    * @param x The value of x.
    * @return The width.
    */
  double GetMean2(double x) override;

  /**
    * Calculate the width of the sedcond Gaussian from the TF parameters and the value of x.
    * @param x The value of x.
    * @return The width.
    */
  double GetSigma2(double x) override;

  /* @} */
};
}  // namespace KLFitter

#endif  // KLFITTER_RESGAUSS_MET_H_
