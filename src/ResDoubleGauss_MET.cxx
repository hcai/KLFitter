/*
 * Copyright (c) 2009--2018, the KLFitter developer team
 *
 * This file is part of KLFitter.
 *
 * KLFitter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * KLFitter is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with KLFitter. If not, see <http://www.gnu.org/licenses/>.
 */

#include "KLFitter/ResDoubleGauss_MET.h"

#include <iostream>

#include "TMath.h"

// ---------------------------------------------------------
KLFitter::ResDoubleGauss_MET::ResDoubleGauss_MET(const char * filename) : KLFitter::ResDoubleGaussBase(filename) { }

// ---------------------------------------------------------
KLFitter::ResDoubleGauss_MET::ResDoubleGauss_MET(std::vector<double> const& parameters) : KLFitter::ResDoubleGaussBase(parameters) { }

// ---------------------------------------------------------
KLFitter::ResDoubleGauss_MET::~ResDoubleGauss_MET() = default;

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::GetMean1(double x) {
  return fParameters[0] + fParameters[1] / x;
}

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::GetSigma1(double x) {
  return fParameters[2] + fParameters[3] / sqrt(x);
}

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::GetAmplitude2(double x) {
  return fParameters[4] + fParameters[5] / x;
}

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::GetMean2(double x) {
  return fParameters[6] + fParameters[7]  / sqrt(x);
}

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::GetSigma2(double x) {
  return fParameters[8] + fParameters[9] * x;
}

// ---------------------------------------------------------
double KLFitter::ResDoubleGauss_MET::logp(double x, double xmeas, bool *good, double sumet) {
  double m1 = GetMean1(sumet);
  double s1 = GetSigma1(sumet);
  double a2 = GetAmplitude2(sumet);
  double m2 = GetMean2(sumet);
  double s2 = GetSigma2(sumet);

  // sanity checks for p2, p3 and p5
  *good = CheckDoubleGaussianSanity(&s1, &a2, &s2);

  double dx = (x - xmeas);

  // calculate double-Gaussian
  const double p =  1./sqrt(2.*M_PI) / (s1 + a2 * s2) * (exp(-(dx-m1)*(dx-m1)/(2 * s1*s1)) + a2 * exp(-(dx-m2)*(dx-m2)/(2 * s2 * s2)));
  return std::log(p);
}
